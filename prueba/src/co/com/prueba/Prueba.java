package co.com.prueba;

import java.util.Arrays;
import java.util.List;


import co.com.prueba.modelo.Respuesta;

public class Prueba {
	
	public static void main(String args[]) {
		List<Integer> lista1 = Arrays.asList(0,3,5,6,7,8,9);
		System.out.println(indiceMagico(lista1));
	}
	
	public static Respuesta indiceMagico(List<Integer> lista) {
		if(null == lista) {
			return Respuesta.responder(false, -1, "La lista enviada es nula");
		}
		if(lista.size() == 0) {
			return Respuesta.responder(false, -1, "La lista enviada esta vacia");
		}
		
		int longitud = lista.size();
		int iterador = longitud / 2;
		int minimo = 0;
		int maximo = longitud;
		boolean finalizado = false;
		int indiceMagico = -1;
		boolean encontrado = false;
		int iteradorAnterior;
		
		while(!finalizado) {
			iteradorAnterior = iterador;
			indiceMagico = lista.get(iterador);
			finalizado = indiceMagico == iterador;
			if(!finalizado) {
				if(indiceMagico > iterador) {
					maximo = iterador;
				}else {
					minimo = iterador;
				}
				iterador = (maximo + minimo) / 2;
				
				if(iteradorAnterior == iterador) {
					finalizado = true;
				}
			}else {
				encontrado = true;
			}
		}
		
		return Respuesta.responder(encontrado, indiceMagico, "");
	}

}
