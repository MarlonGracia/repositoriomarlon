package co.com.prueba.test;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import co.com.prueba.Prueba;
import co.com.prueba.modelo.Respuesta;


public class PruebaTest {
	
	@Test
	public void respuestaIndice0() {
		List<Integer> lista = Arrays.asList(0,3,5,6,7,8,9);
		
		Respuesta respuesta = Prueba.indiceMagico(lista);
		assertEquals(respuesta.getIndiceMagico(), 0);
		assertEquals(respuesta.isPoseeIndiceMagico(), true);
	}
	
	@Test
	public void respuestaIndice1() {
		List<Integer> lista = Arrays.asList(-10,1,4,6,7,8,9);;
		
		Respuesta respuesta = Prueba.indiceMagico(lista);
		assertEquals(respuesta.getIndiceMagico(), 1);
		assertEquals(respuesta.isPoseeIndiceMagico(), true);
	}
	
	@Test
	public void respuestaIndice2() {
		List<Integer> lista = Arrays.asList(-10,-8,2,4,5,9,10);
		
		Respuesta respuesta = Prueba.indiceMagico(lista);
		assertEquals(respuesta.getIndiceMagico(), 2);
		assertEquals(respuesta.isPoseeIndiceMagico(), true);
	}
	
	@Test
	public void respuestaIndice3() {
		List<Integer> lista = Arrays.asList(-10,-8,-3,3,6,9,10);
		
		Respuesta respuesta = Prueba.indiceMagico(lista);
		assertEquals(respuesta.getIndiceMagico(), 3);
		assertEquals(respuesta.isPoseeIndiceMagico(), true);
	}
	
	@Test
	public void respuestaIndice4() {
		List<Integer> lista = Arrays.asList(-10,-8,-3,0,4,9,10);
		
		Respuesta respuesta = Prueba.indiceMagico(lista);
		assertEquals(respuesta.getIndiceMagico(), 4);
		assertEquals(respuesta.isPoseeIndiceMagico(), true);
	}
	
	@Test
	public void respuestaIndice5() {
		List<Integer> lista = Arrays.asList(-10,-8,-3,0,1,5,10);
		
		Respuesta respuesta = Prueba.indiceMagico(lista);
		assertEquals(respuesta.getIndiceMagico(), 5);
		assertEquals(respuesta.isPoseeIndiceMagico(), true);
	}
	
	@Test
	public void respuestaIndice6() {
		List<Integer> lista = Arrays.asList(-10,-8,-3,0,1,3,6);
		
		Respuesta respuesta = Prueba.indiceMagico(lista);
		assertEquals(respuesta.getIndiceMagico(), 6);
		assertEquals(respuesta.isPoseeIndiceMagico(), true);
	}
	
	@Test
	public void respuestaSinIndice() {
		List<Integer> lista = Arrays.asList(-10,-8,-3,0,1,3,7);
		
		Respuesta respuesta = Prueba.indiceMagico(lista);
		assertEquals(respuesta.getIndiceMagico(), -1);
		assertEquals(respuesta.isPoseeIndiceMagico(), false);
		assertEquals(respuesta.getRespuesta(), "El array no posee indice magico");
	}
	
	@Test
	public void respuestaNula() {
		List<Integer> lista = null;
		
		Respuesta respuesta = Prueba.indiceMagico(lista);
		assertEquals(respuesta.getIndiceMagico(), -1);
		assertEquals(respuesta.isPoseeIndiceMagico(), false);
		assertEquals(respuesta.getRespuesta(), "La lista enviada es nula");
	}
	
	@Test
	public void respuestaVacia() {
		List<Integer> lista = Arrays.asList();
		
		Respuesta respuesta = Prueba.indiceMagico(lista);
		assertEquals(respuesta.getIndiceMagico(), -1);
		assertEquals(respuesta.isPoseeIndiceMagico(), false);
		assertEquals(respuesta.getRespuesta(), "La lista enviada esta vacia");
	}
	

}
