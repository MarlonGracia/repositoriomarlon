package co.com.prueba.modelo;

public class Respuesta {
	
	private boolean poseeIndiceMagico;
	private int indiceMagico;
	private String respuesta;
	
	public Respuesta(boolean poseeIndiceMagico, int indiceMagico, String respuesta) {
		super();
		this.poseeIndiceMagico = poseeIndiceMagico;
		this.indiceMagico = indiceMagico;
		this.respuesta = respuesta;
	}
	
	public boolean isPoseeIndiceMagico() {
		return poseeIndiceMagico;
	}
	public void setPoseeIndiceMagico(boolean poseeIndiceMagico) {
		this.poseeIndiceMagico = poseeIndiceMagico;
	}
	public int getIndiceMagico() {
		return indiceMagico;
	}
	public void setIndiceMagico(int indiceMagico) {
		this.indiceMagico = indiceMagico;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	
	@Override
	public String toString() {
		return "Respuesta [poseeIndiceMagico=" + poseeIndiceMagico + ", indiceMagico=" + indiceMagico + ", respuesta="
				+ respuesta + "]";
	}

	public static Respuesta responder(boolean poseeIndice, int indice, String mensaje) {
		String mensajeRespuesta = "";
		int indiceRespuesta = poseeIndice ? indice : -1;
		
		if(!poseeIndice) {
			mensajeRespuesta = null != mensaje && mensaje.trim().length() > 0 ? mensaje:"El array no posee indice magico";
		}
		
		return new Respuesta(poseeIndice, indiceRespuesta, mensajeRespuesta);
		
	}
}
